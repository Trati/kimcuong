﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Ads : MonoBehaviour
{
    public static Ads ad;
    public Image customBanner;
    public Image customFullScreen;
    public bool isOpenFullScreen = false;

    bool isShowCustomAds = true;
    CustomAds currentBanner;
    CustomAds curentFullScreen;
    Texture2D fullImg;
    Texture2D bannerImg;
    int index = 0;
    int time = 0;
    

    string ModeName;        // mode of game - Arcede or classic
    void Start()
    {
        ad = this;
        if (PLayerInfo.MODE == 1)
            ModeName = "ARCADE ";
        else
            ModeName = "CLASSIC ";
        MusicController.Music.BG_play();

        // check show admob interstitial or no
        if (!Timer.timer.isreq)
        {
            // GoogleMobileAdsScript.advertise.RequestInterstitial();
            Timer.timer.isreq = true;
        }

        ShowBanner();
        RequestFullScreen();

        // request Google Analytics
        AdmobGA.load.GA.LogScreen(ModeName + "Level: " + PLayerInfo.MapPlayer.Level);
        Firebase.Analytics.FirebaseAnalytics.LogEvent("GameScene-Level: " + PLayerInfo.MapPlayer.Level);
    }

    void Update()
    {
        if (isShowCustomAds)
        {
            time++;

            if (time == 3600)
            {
                time = 0;
                StartCoroutine(ShowCustomAds());
            }
        }
    }

    void RequestFullScreen()
    {
        if (GoogleMobileAdsScript.advertise.customPopups.Count > 0)
        {
            StartCoroutine(RequestCustomFull());
        }
        else
        {
            GoogleMobileAdsScript.advertise.RequestInterstitial();
        }
    }

    IEnumerator RequestCustomFull()
    {
        curentFullScreen = GoogleMobileAdsScript.advertise.customPopups[Random.Range(0, GoogleMobileAdsScript.advertise.customPopups.Count)];
        fullImg = new Texture2D(1, 1, TextureFormat.RGB24, false);
        yield return IELoadImageFullScreen(curentFullScreen.imgURl, fullImg);

        if (fullImg != null)
        {
            ShowFullScreen(customFullScreen, fullImg);
        }
        else
        {
            GoogleMobileAdsScript.advertise.RequestInterstitial();
        }
    }

    public void ShowFullscreen()
    {
        HideBanner();

        if (fullImg != null)
        {
            isOpenFullScreen = true;
            customFullScreen.gameObject.SetActive(true);
            Firebase.Analytics.FirebaseAnalytics.LogEvent("Show custom interstitial");
        }
        else
        {
            customFullScreen.gameObject.SetActive(false);
            GoogleMobileAdsScript.advertise.ShowInterstitial();
        }
    }

    void ShowBanner()
    {
        if (GoogleMobileAdsScript.advertise.customBanners.Count > 0)
        {
            isShowCustomAds = true;
            StartCoroutine(ShowCustomAds());
        }
        else
        {
            // show banner
            isShowCustomAds = false;
            GoogleMobileAdsScript.advertise.ShowBanner();
        }
    }

    IEnumerator ShowCustomAds()
    {
        currentBanner = GoogleMobileAdsScript.advertise.customBanners[index];
        index++;
        if (index >= GoogleMobileAdsScript.advertise.customBanners.Count)
            index = 0;

        bannerImg = new Texture2D(1, 1, TextureFormat.RGB24, false);
        yield return IELoadImageFullScreen(currentBanner.imgURl, bannerImg);

        if (bannerImg != null)
        {
            ShowFullScreen(customBanner, bannerImg);
            customBanner.gameObject.SetActive(true);

            Firebase.Analytics.FirebaseAnalytics.LogEvent("Show custom banner");
        }
        else
        {
            customBanner.gameObject.SetActive(false);
        }
    }

    void ShowFullScreen(Image obj, Texture2D texture)
    {
        if (texture != null)
        {
            Sprite sprite = Sprite.Create(texture,
                                   new Rect(0, 0, texture.width, texture.height),
                                   new Vector2(0.5f, 0.5f));
            obj.sprite = sprite;
        }
    }

    IEnumerator IELoadImageFullScreen(string URL, Texture2D texture)
    {
        WWW imgurl = new WWW(URL);
        yield return imgurl;

        if (imgurl.error == null)
        {
            texture.LoadImage(imgurl.bytes);
            imgurl.Dispose();
        }
        else
        {
            Debug.Log(imgurl.error);
        }
    }

    public void ClickImageFull()
    {
        Application.OpenURL(curentFullScreen.target);
        Firebase.Analytics.FirebaseAnalytics.LogEvent("Click custom interstitial");
        customFullScreen.gameObject.SetActive(false);
        isOpenFullScreen = false;
    }

    public void ClickImageBanner()
    {
        Application.OpenURL(currentBanner.target);
        Firebase.Analytics.FirebaseAnalytics.LogEvent("Click custom banner");
    }

    public void ExitFullScreen()
    {
        customFullScreen.gameObject.SetActive(false);
        isOpenFullScreen = false;
    }

    public void HideBanner()
    {
        customBanner.gameObject.SetActive(false);
        GoogleMobileAdsScript.advertise.HideBanner();
    }
}
