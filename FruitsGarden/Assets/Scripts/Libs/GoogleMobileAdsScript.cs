﻿using System;
using UnityEngine;
using GoogleMobileAds;
using GoogleMobileAds.Api;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Advertisements.MiniJSON;
using System.Threading.Tasks;

[System.Serializable]
public class CustomAds
{
    public string imgURl;
    public string target;
}
public class GoogleMobileAdsScript : MonoBehaviour
{

    public static GoogleMobileAdsScript advertise;      // instance of  GoogleMobileAdsScript
    public string bundleId = "com.wintek.fruitgarden";
    private BannerView bannerView;                      // banner ads
    private InterstitialAd interstitial;                // interstitial ads

    public string androidBanner;                        // unit id banner for android
    public string androidInterstitial;                  // unit id instersitial for android

    public string iosBanner;                            // unit id banner for IOS
    public string iosInterstitial;                      // unit id instersitial for IOS

    public AdSize adSize = AdSize.Banner;               // size and type of banner ads
    public AdPosition adPosition = AdPosition.Bottom;   // position of banner ads
    public bool isOpenFullscreen = false;

    public List<CustomAds> customBanners;
    public List<CustomAds> customPopups;
    public List<CustomAds> customVideos;
    public int time_to_show_popup = 2;
    public int time_to_show_video = 2;

    private List<int> times_to_show_popup_list = new List<int>();
    private List<int> times_to_show_video_list = new List<int>();

    Firebase.DependencyStatus dependencyStatus = Firebase.DependencyStatus.UnavailableOther;

    void Awake()
    {
        if (advertise == null)
        {
            // Makes the object target not be destroyed automatically when loading a new scene
            DontDestroyOnLoad(gameObject);
            advertise = this;
        }
        else if (advertise != this)
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        dependencyStatus = Firebase.FirebaseApp.CheckDependencies();
        if (dependencyStatus != Firebase.DependencyStatus.Available)
        {
            Firebase.FirebaseApp.FixDependenciesAsync().ContinueWith(task =>
            {
                dependencyStatus = Firebase.FirebaseApp.CheckDependencies();
                if (dependencyStatus == Firebase.DependencyStatus.Available)
                {
                    InitializeFirebase();
                }
                else
                {
                    Debug.LogError(
                        "Could not resolve all Firebase dependencies: " + dependencyStatus);
                }
            });
        }
        else
        {
            InitializeFirebase();
        }

        FetchData();

        //Firebase.RemoteConfig.FirebaseRemoteConfig.FetchAsync();
        //foreach (string str in Firebase.RemoteConfig.FirebaseRemoteConfig.Keys)
        //    Debug.Log("BBBB" + str);
        //Firebase.RemoteConfig.ConfigValue  url = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("ads_link");
        //Debug.Log("XXXXXXXX: " + url.StringValue);
        // StartCoroutine( GetAPI("http://ourfashion.top/admob.php"));
    }

    void InitializeFirebase()
    {
        Firebase.Analytics.FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
        System.Collections.Generic.Dictionary<string, object> defaults =
          new System.Collections.Generic.Dictionary<string, object>();

        // These are the values that are used if we haven't fetched data from the
        // server
        // yet, or if we ask for values that the server doesn't have:
        defaults.Add("ads_link", "http://ourfashion.top/admob.php");
        Firebase.RemoteConfig.FirebaseRemoteConfig.SetDefaults(defaults);
    }
    public void FetchData()
    {
        System.Threading.Tasks.Task fetchTask = Firebase.RemoteConfig.FirebaseRemoteConfig.FetchAsync(
            TimeSpan.Zero);
        fetchTask.ContinueWith(FetchComplete);
    }

    void FetchComplete(Task fetchTask)
    {
        if (fetchTask.IsCanceled)
        {
            Debug.Log("Fetch canceled.");
        }
        else if (fetchTask.IsFaulted)
        {
            Debug.Log("Fetch encountered an error.");
        }
        else if (fetchTask.IsCompleted)
        {
            Debug.Log("Fetch completed successfully!");
        }

        switch (Firebase.RemoteConfig.FirebaseRemoteConfig.Info.LastFetchStatus)
        {
            case Firebase.RemoteConfig.LastFetchStatus.Success:
                Firebase.RemoteConfig.FirebaseRemoteConfig.ActivateFetched();
                Debug.Log("Remote data loaded and ready.");

                //foreach (string str in Firebase.RemoteConfig.FirebaseRemoteConfig.Keys)
                //     Debug.Log("BBBB" + str);
                Firebase.RemoteConfig.ConfigValue url = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("ads_link");
                StartCoroutine(GetAPI(url.StringValue));//"http://ourfashion.top/admob.php"));
                break;
            case Firebase.RemoteConfig.LastFetchStatus.Failure:
                switch (Firebase.RemoteConfig.FirebaseRemoteConfig.Info.LastFetchFailureReason)
                {
                    case Firebase.RemoteConfig.FetchFailureReason.Error:
                        Debug.Log("Fetch failed for unknown reason");
                        break;
                    case Firebase.RemoteConfig.FetchFailureReason.Throttled:
                        Debug.Log("Fetch throttled until " +
                                 Firebase.RemoteConfig.FirebaseRemoteConfig.Info.ThrottledEndTime);
                        break;
                }
                break;
            case Firebase.RemoteConfig.LastFetchStatus.Pending:
                Debug.Log("Latest Fetch call still pending.");
                break;
        }
    }

    IEnumerator GetAPI(string url)
    {
        if (string.IsNullOrEmpty(url))
            yield break;

        WWW www = new WWW(url);
        yield return www;

        if (www.error == null)
        {
            var dict = Json.Deserialize(www.text) as Dictionary<string, object>;
            var banners = dict["banner"] as List<object>;
            var popups = dict["popup"] as List<object>;
            var videos = dict["video"] as List<object>;
            var times = dict["time"] as Dictionary<string, object>;
            var popupshow = times["popup"] as Dictionary<string, object>;
            var videoshow = times["video"] as Dictionary<string, object>;

            // banner
            for (int i = 0; i < banners.Count; i++)
            {
                var banner = banners[i] as Dictionary<string, object>;
                CustomAds custom = new CustomAds();

                if (banner.ContainsKey("image"))
                    custom.imgURl = (string)banner["image"];

                if (banner.ContainsKey("target"))
                    custom.target = (string)banner["target"];

                customBanners.Add(custom);
            }

            // popup
            for (int i = 0; i < popups.Count; i++)
            {
                var popup = popups[i] as Dictionary<string, object>;
                CustomAds custom = new CustomAds();

                if (popup.ContainsKey("image"))
                    custom.imgURl = (string)popup["image"];

                if (popup.ContainsKey("target"))
                    custom.target = (string)popup["target"];

                customPopups.Add(custom);
            }

            // video
            for (int i = 0; i < videos.Count; i++)
            {
                var video = videos[i] as Dictionary<string, object>;
                CustomAds custom = new CustomAds();

                if (video.ContainsKey("image"))
                    custom.imgURl = (string)video["image"];

                if (video.ContainsKey("target"))
                    custom.target = (string)video["target"];

                customVideos.Add(custom);
            }

            if (popupshow.ContainsKey(bundleId))
            {
                var list = popupshow[bundleId] as List<object>;
                for (int i = 0; i < list.Count; i++)
                {
                    int tmp = int.Parse(list[i].ToString());
                      times_to_show_popup_list.Add(tmp);
                }

                if (times_to_show_popup_list.Count > 0) ;
                time_to_show_popup = times_to_show_popup_list[UnityEngine.Random.Range(0, times_to_show_popup_list.Count)];


            }

            if (videoshow.ContainsKey(bundleId))
            {
                var list = videoshow[bundleId] as List<object>;
                for (int i = 0; i < list.Count; i++)
                {
                    int tmp = int.Parse(list[i].ToString());
                    times_to_show_video_list.Add(tmp);
                }
                if (times_to_show_video_list.Count > 0) ;
                time_to_show_video = times_to_show_video_list[UnityEngine.Random.Range(0, times_to_show_video_list.Count)];
            }
        }
    }



    /// <summary>
    /// Create a banner 
    /// </summary>
    public void RequestBanner()
    {
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
		string adUnitId = androidBanner;
#elif UNITY_IPHONE
		string adUnitId = iosBanner;
#else
		string adUnitId = "unexpected_platform";
#endif
        try
        {
            // Create a 320x50 banner at the top of the screen.
            this.bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Bottom);

            // Register for ad events.
            //this.bannerView.OnAdLoaded += this.HandleAdLoaded;
            //this.bannerView.OnAdFailedToLoad += this.HandleAdFailedToLoad;
            //this.bannerView.OnAdOpening += this.HandleAdOpened;
            //this.bannerView.OnAdClosed += this.HandleAdClosed;
            //this.bannerView.OnAdLeavingApplication += this.HandleAdLeftApplication;

            // Load a banner ad.
            this.bannerView.LoadAd(this.CreateAdRequest());
        }
        catch { }
    }
    /// <summary>
    /// create an interstitial.
    /// </summary>
    public void RequestInterstitial()
    {
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
		string adUnitId = androidInterstitial;
#elif UNITY_IPHONE
		string adUnitId = iosInterstitial;
#else
		string adUnitId = "unexpected_platform";
#endif
        try
        {
            // Create an interstitial.
            this.interstitial = new GoogleMobileAds.Api.InterstitialAd(adUnitId);

            // Register for ad events.
            this.interstitial.OnAdLoaded += this.HandleInterstitialLoaded;
            this.interstitial.OnAdFailedToLoad += this.HandleInterstitialFailedToLoad;
            this.interstitial.OnAdOpening += this.HandleInterstitialOpened;
            this.interstitial.OnAdClosed += this.HandleInterstitialClosed;
            this.interstitial.OnAdLeavingApplication += this.HandleInterstitialLeftApplication;

            // Load an interstitial ad.
            this.interstitial.LoadAd(this.CreateAdRequest());
        }
        catch { }
    }

    /// <summary>
    /// Create a request ads
    /// </summary>
    /// <returns>Returns an ad request with custom ad targeting.</returns>
    private AdRequest CreateAdRequest()
    {
        return new AdRequest.Builder()
           .AddTestDevice(AdRequest.TestDeviceSimulator)
           .AddTestDevice("0123456789ABCDEF0123456789ABCDEF")
           .AddKeyword("game")
           .SetGender(Gender.Male)
           .SetBirthday(new DateTime(1985, 1, 1))
           .TagForChildDirectedTreatment(false)
           .AddExtra("color_bg", "9B30FF")
           .Build();
    }

    /// <summary>
    /// Show interstitial if loaded
    /// </summary>
    public void ShowInterstitial()
    {
        try
        {
            if (interstitial.IsLoaded())
            {
                interstitial.Show();
            }
        }
        catch { }
    }

    /// <summary>
    /// Show banner
    /// </summary>
    public void ShowBanner()
    {
        try
        {

            bannerView.Show();
        }
        catch { }
    }

    /// <summary>
    /// hidden banner on scene
    /// </summary>
    public void HideBanner()
    {

        if (bannerView != null)
            bannerView.Hide();
    }

    /// <summary>
    /// destroy banner method
    /// </summary>
    public void DestroyBanner()
    {
        bannerView.Destroy();
    }

    #region Banner callback handlers

    public void HandleAdLoaded(object sender, EventArgs args)
    {
        print("HandleAdLoaded event received.");
    }

    public void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        print("HandleFailedToReceiveAd event received with message: " + args.Message);
    }

    public void HandleAdOpened(object sender, EventArgs args)
    {
        print("HandleAdOpened event received");
    }

    void HandleAdClosing(object sender, EventArgs args)
    {
        print("HandleAdClosing event received");
    }

    public void HandleAdClosed(object sender, EventArgs args)
    {
        print("HandleAdClosed event received");
    }

    public void HandleAdLeftApplication(object sender, EventArgs args)
    {
        print("HandleAdLeftApplication event received");
    }

    #endregion

    #region Interstitial callback handlers

    public void HandleInterstitialLoaded(object sender, EventArgs args)
    {
        print("HandleInterstitialLoaded event received.");
    }

    public void HandleInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        print("HandleInterstitialFailedToLoad event received with message: " + args.Message);
    }

    public void HandleInterstitialOpened(object sender, EventArgs args)
    {
        isOpenFullscreen = true;
        print("HandleInterstitialOpened event received");
    }

    void HandleInterstitialClosing(object sender, EventArgs args)
    {
        isOpenFullscreen = false;
        print("HandleInterstitialClosing event received");
    }

    public void HandleInterstitialClosed(object sender, EventArgs args)
    {
        print("HandleInterstitialClosed event received");
    }

    public void HandleInterstitialLeftApplication(object sender, EventArgs args)
    {
        print("HandleInterstitialLeftApplication event received");
    }

    #endregion
}
